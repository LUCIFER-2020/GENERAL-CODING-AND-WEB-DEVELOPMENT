### Hi there, I'm Renesh - aka LUCIFER👋
[![LICENSE: MIT](https://img.shields.io/website?label=LICENCSE-MIT&style=for-the-badge&url=https%3A%2F%2F.github.com/LUCIFER-2020/PROJECT/blob/main/LICENSE)](https://github.com/LUCIFER-2020/PROJECT/blob/main/LICENSE)
[![Twitter Follow](https://img.shields.io/twitter/follow/SuvarnaRenesh?color=1DA1F2&logo=twitter&style=for-the-badge)](https://mobile.twitter.com/SuvarnaRenesh)
[![Website](https://img.shields.io/website?label=W3SCHOOLS.com&style=for-the-badge&url=https%3A%2F%2F.w3schools.com/)](https://www.w3schools.com/)

## I'm a Student, Son, Web Developer, and Hacker!!

### 📃One programmer makes the program, others are just looking for the source code.📃
- 🔭 I usually write my codes with VS Code.
- 🌱 I’m currently learning everything 🤣
- 👯 I’m looking to collaborate with other content creators
- 🥅 2021 Goals: Contribute more to Open Source projects
- ⚡ Fun fact: I love to draw and play cook / cricket


### Spotify Playing 🎧

[<img src="https://now-playing-codestackr.vercel.app/api/spotify-playing" alt="codeSTACKr Spotify Playing" width="350" />](https://open.spotify.com/user/swyqyimdc12jajde4vpwd2x1b)

### Connect with me:

[<img align="left" alt="codeSTACKr | Website" width="22px" src="https://visualpharm.com/assets/80/Globe%20Earth-595b40b65ba036ed117d1cfe.svg" />][Website]
[<img align="left" alt="codeSTACKr | Twitter" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />][twitter]
[<img align="left" alt="codeSTACKr | Instagram" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/instagram.svg" />][instagram]


<br />


---


</details>


[website]: https://www.w3schools.com
[twitter]: https://mobile.twitter.com/SuvarnaRenesh
[instagram]: https://www.instagram.com/renesh_2020
